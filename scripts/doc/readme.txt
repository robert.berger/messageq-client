==  update from upstream ==
>> git co master

>> git remote -v
official-upstream       git@gitlab.com:poky_noty/messageq-client.git (fetch)
official-upstream       git@gitlab.com:poky_noty/messageq-client.git (push)
origin  git@gitlab.com:robert.berger/messageq-client.git (fetch)
origin  git@gitlab.com:robert.berger/messageq-client.git (push)

>> git fetch official-upstream
remote: Enumerating objects: 14, done.
remote: Counting objects: 100% (14/14), done.
remote: Compressing objects: 100% (10/10), done.
remote: Total 10 (delta 5), reused 0 (delta 0)
Unpacking objects: 100% (10/10), done.
From gitlab.com:poky_noty/messageq-client
 * [new branch]      master     -> official-upstream/master

>> git branch -a
* master
  remotes/official-upstream/master
  remotes/origin/HEAD -> origin/master
  remotes/origin/master

>> git diff master..official-upstream/master !(scripts)
diff --git a/messageqclient/__main__.py b/messageqclient/__main__.py
new file mode 100644
index 0000000..aefb1e7
--- /dev/null
+++ b/messageqclient/__main__.py
@@ -0,0 +1,4 @@
+from .redis.simple_connection import establish_single_connection
+
+def main():
+    establish_single_connection()
diff --git a/messageqclient/redis/simple_set.py b/messageqclient/redis/simple_set.py
new file mode 100644
index 0000000..2a6f2c1
--- /dev/null
+++ b/messageqclient/redis/simple_set.py
@@ -0,0 +1,8 @@
+import redis
+
+from . import *
+
+def get_all_members_of_set( client, key ):
+    logger.debug( "Good Morning" )
+    response = client.smembers( key )
+    logger.debug( "Good Night" )
diff --git a/setup.py b/setup.py
index 5f82baa..b56288f 100644
--- a/setup.py
+++ b/setup.py
@@ -20,5 +20,10 @@ setup(
         'Programming Language :: Python :: 3.4',
         'Topic :: Storage :: Memory :: Key-Value :: Pub/Sub :: Listener :: Topic',
     ],
+    entry_points={
+        'console_scripts': [
+            'redis_client=messageqclient.__main__:main'
+        ]
+    },
...

>> git rebase official-upstream/master
First, rewinding head to replay your work on top of it...
Applying: scripts
Applying: git magic
Applying: more doc
Applying: doc

>> git diff master..official-upstream/master !(scripts)

Now there are no more differences !
